import {applyMiddleware, combineReducers, createStore} from "redux";
import {todoReducer} from "../features/Todo/state/todoReducer";
import {composeWithDevTools} from "redux-devtools-extension";
import {todoMiddleware} from "../features/Todo/state/todoMiddleware";

const appReducers = combineReducers({
    todo: todoReducer
})

export default createStore(
    appReducers,
    composeWithDevTools(
        applyMiddleware(todoMiddleware)
    )
)