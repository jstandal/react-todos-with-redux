import styles from './AppContainer.module.css'

const AppContainer = props => {
    return (
        <div className={styles.AppContainer}>
            {props.children}
        </div>
    )
}
export default AppContainer