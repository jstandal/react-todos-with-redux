import {useSelector} from "react-redux";
import styles from './Navbar.module.css'
import {NavLink} from "react-router-dom";

const Navbar = () => {

    const {todos} = useSelector(state => state.todo)
    return (
        <nav className={styles.Navbar}>
            <ul className={ styles.NavbarMenu }>
                <li className={styles.NavbarMenuItem}>
                    <NavLink activeClassName={styles.NavbarMenuItemActive} to="/todos">Todos</NavLink>
                </li>
            </ul>
            <ul className={ styles.NavbarMenu }>
                <li className={styles.NavbarTodoCount}>Todos: { todos.length }</li>
            </ul>
        </nav>
    )
}
export default Navbar