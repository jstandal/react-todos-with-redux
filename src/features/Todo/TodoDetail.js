import User from "../User/User";
import AppContainer from "../../hoc/AppContainer";
import {useEffect} from "react";
import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";

const TodoDetail = () => {
    const { id } = useParams()
    const { todo } = useSelector(state => state.todoDetail)

    useEffect(() => {
        // dispatch action - fetchTodoDetail based in id
        // In the middleware - Side Effect -> Trigger get userActionFetch(userId) in todoDetailMiddleware
    }, [])

    return (
        <AppContainer>
            <h4>Todo detail</h4>
            {
                todo && <User userId={ todo.userId }/>
            }
        </AppContainer>
    )
}
export default TodoDetail