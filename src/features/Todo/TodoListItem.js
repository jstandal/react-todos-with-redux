import styles from './TodoListItem.module.css'

const TodoListItem = ({ todo, clicked }) => {
    return (
        <li className={styles.TodoListItem} onClick={ () => clicked(todo) }>
            <h4 className={styles.ItemTitle}>{ todo.title }</h4>
            <p className={styles.ItemCompleted}>Is Done: { todo.completed ? 'Yes' : 'No' }</p>
        </li>
    )
}
export default TodoListItem