export const ACTION_TODO_FETCH = '[todo] FETCH'
export const ACTION_TODO_SET = '[todo] SET'
export const ACTION_TODO_SET_ERROR = '[todo] SET_ERROR'



export const todoActionFetch = () => ({
    type: ACTION_TODO_FETCH
})

export const todoActionSet = payload => ({
    type: ACTION_TODO_SET,
    payload // An array of todos
})

export const todoActionError = payload => ({
    type: ACTION_TODO_SET_ERROR,
    payload // error message
})

